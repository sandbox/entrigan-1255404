<?php

/*
 * @file
 * 
 * Plugin to allow page manager to take over the profile category page.
 *
 */

 //@TODO: Make it clear that this is for the page provided by profile2.  Core profile does not create profile edit pages.
function ctools_pages_user_profile_edit_page_manager_tasks() {
  if(module_exists('profile2')){
    return array(
      // This is a 'page' task and will fall under the page admin UI
      'task type' => 'page',
  
      'title' => t('User Edit Profile'),
      'admin title' => t('User Edit Profile'),
      'admin description' => t('When enabled, this overrides the default Drupal behavior for the user register page at <em>/user/register</em>.'),
      'admin path' => 'user/%user/edit/%profile',
  
      // Menu hooks so that we can alter the node/%node menu entry to point to us.
      'hook menu alter' => 'ctools_pages_user_profile_edit_menu_alter',
   
      // This is task uses 'context' handlers and must implement these to give the
      // handler data it needs.
      'handler type' => 'context',
      'get arguments' => 'ctools_pages_user_profile_get_arguments',
      'get context placeholders' => 'ctools_pages_user_profile_get_contexts',

  
      // Allow this to be enabled or disabled:
      'disabled' => variable_get('ctools_pages_user_profile_edit_disabled', TRUE),
      'enable callback' => 'ctools_pages_user_profile_edit_enable',
    );
  }
}

/**
 * Callback defined by ctools_pages_user_profile_edit_page_manager_tasks().
 *
 * Alter the node edit input so that node edit comes to us rather than the
 * normal node edit process.
 */
function ctools_pages_user_profile_edit_menu_alter(&$items, $task) {
  if (variable_get('ctools_pages_user_profile_edit_disabled', TRUE)) {
    return;
  }
  
  if (($categories = _user_categories()) && (count($categories) > 1)) {
    foreach ($categories as $key => $category) {
      // 'account' is already handled by the MENU_DEFAULT_LOCAL_TASK.
      if ($category['name'] != 'account') {
        $callback = $items['user/%user_category/edit/' . $category['name']]['page callback'];
        if ($callback == 'drupal_get_form' || variable_get('page_manager_override_anyway', FALSE)) {
          $items['user/%user_category/edit/' . $category['name']]['page callback']='ctools_pages_user_profile_edit';
          $items['user/%user_category/edit/' . $category['name']]['file path'] = $task['path'];
          $items['user/%user_category/edit/' . $category['name']]['file'] = $task['file'];
          $success[] = 'user/%user_category/edit/' . $category['name'];
        }
        else{
          $failed[] = 'user/%user_category/edit/' . $category['name'];
        }
      }
    }
  }
  if(!isset($success)){
    variable_set('ctools_pages_user_profile_edit_disabled', TRUE);
  }
  if(isset($failed)){
    if (!empty($GLOBALS['ctools_pages_enabling_user_profile_edit'])) {
      $failed_list = implode(',', $failed);
      drupal_set_message(t('Page manager module is unable to override the following profile edit pages: <em>%callbacks</em>.', array('%callbacks' => $failed_list)), 'warning');
    }
  }
   

}

/**
 * Callback to get arguments provided by this task handler.
 *
 * Since this is the profile category eidt and there is no UI on the arguments, we
 * create dummy arguments that contain the needed data.
 */
function ctools_pages_user_profile_get_arguments($task, $subtask_id) {
  return array(
    array(
      'keyword' => 'profile_category',
      'identifier' => t('Profile being edited'),
      'id' => 1,
      'name' => 'profile_category',
      'settings' => array(),
    ),
  );
}

/**
 * Callback to get context placeholders provided by this handler.
 */
function ctools_pages_user_profile_get_contexts($task, $subtask_id) {
  return ctools_context_get_placeholders_from_argument(ctools_pages_user_profile_get_arguments($task, $subtask_id));
}

/**
 * Entry point for our overridden node edit.
 *
 * This function asks its assigned handlers who, if anyone, would like
 * to run with it. If no one does, it passes through to Drupal core's
 * node edit, which is node_page_edit().
 */
function ctools_pages_user_profile_edit($form_id, $account, $category) {
  // Load my task plugin
  $task = page_manager_get_task('user_profile_edit');

  ctools_include('context');
  ctools_include('context-task-handler');
  $profile = array('category' => $category, 'account' => $account);
  $contexts = ctools_context_handler_get_task_contexts($task, '', array($profile, $account));
  
  $arg = array($profile);
  
  $output = ctools_context_handler_render($task, '', $contexts, $arg);
  if ($output === FALSE) {
    // Fall back!
    // We've already built the form with the context, so we can't build it again, or
    // form_clean_id will mess up our ids. But we don't really need to, either:
    $context = reset($contexts);
    $output = $context->form;
  }
  
  return $output;

/*OLD CODE
  $output = ctools_context_handler_render($task, '', array(), array());
  if ($output !== FALSE) {
     return $output;
  }

  module_load_include('inc', 'user', 'user.pages');
  foreach (module_implements('page_manager_override') as $module) {
    $call = $module . '_page_manager_override';
    if (($rc = $call('user_profile_edit')) && function_exists($rc)) {
      $function = $rc;
      break;
    }
  }
  
  if(isset($function)){
    return $function;
  }
  // Otherwise, fall back.
  return ctools_pages_get_form($form_id, array($account, $category), array('module' => 'user', 'file' => 'user.pages'));
 * */
}

/**
 * Callback to enable/disable the page from the UI.
 */
function ctools_pages_user_profile_edit_enable($cache, $status) {
  variable_set('ctools_pages_user_profile_edit_disabled', $status);
  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['ctools_pages_enabling_user_profile_edit'] = TRUE;
  }
}
