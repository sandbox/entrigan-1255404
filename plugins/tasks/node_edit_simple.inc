<?php

/**
 * @file
 * Provides a simplified node edit task.
 * 
 * See issue #1139918.  Should be obsolete once that gets resolved.
 */
function ctools_pages_node_edit_simple_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',
    'title' => t('Node Add/Edit Template'),
    'admin title' => t('Node edit template'),
    'admin description' => t('Simplified without the form context.'),
    'admin path' => 'node/%node/edit',

    // Callback to add items to the page managertask administration form:
    'task admin' => 'ctools_pages_node_edit_simple_task_admin',

    'hook menu' => 'ctools_pages_node_edit_simple_menu',
    'hook menu alter' => 'ctools_pages_node_edit_simple_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context', // handler type -- misnamed
    'get arguments' => 'ctools_pages_node_edit_simple_get_arguments',
    'get context placeholders' => 'ctools_pages_node_edit_simple_get_contexts',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('ctools_pages_node_edit_simple_disabled', TRUE),
    'enable callback' => 'ctools_pages_node_edit_simple_enable',
  );
}

function ctools_pages_node_edit_simple_menu_alter(&$items, $task) {
  if (variable_get('ctools_pages_node_edit_simple_disabled', TRUE)) {
    return;
  }

  $callback = $items['node/%node/edit']['page callback'];
  // Override the node edit handler for our purpose.
  if ($callback == 'node_page_edit' || variable_get('page_manager_override_anyway', FALSE)) {
    $items['node/%node/edit']['page callback'] = 'ctools_pages_node_edit_simple';
    $items['node/%node/edit']['file path'] = $task['path'];
    $items['node/%node/edit']['file'] = $task['file'];
  }
  else {
    variable_set('ctools_pages_node_edit_simple_disabled', TRUE);
    if (!empty($GLOBALS['pctools_pages_enabling_node_edit_simple'])) {
      drupal_set_message(t('Page manager module is unable to enable node/%node/edit because some other module already has overridden with %callback.', array('%callback' => $callback)), 'warning');
    }
    return;
  }

  // Also catch node/add handling:
  foreach (node_type_get_types() as $type) {
    $path = 'node/add/' . str_replace('_', '-', $type->type);
    if ($items[$path]['page callback'] != 'node_add') {
      if (!empty($GLOBALS['ctools_pages_enabling_node_edit_simple'])) {
        drupal_set_message(t('Page manager module is unable to override @path because some other module already has overridden with %callback. Node edit will be enabled but that edit path will not be overridden.', array('@path' => $path, '%callback' => $items[$path]['page callback'])), 'warning');
      }
      continue;
    }

    $items[$path]['page callback'] = 'ctools_pages_node_add_simple';
    $items[$path]['file path'] = $task['path'];
    $items[$path]['file'] = $task['file'];
    $items[$path]['page arguments'] = array($type->type);
  }
}

function ctools_pages_node_edit_simple($node) {
    // Load my task plugin
  $task = page_manager_get_task('node_edit_simple');

  // Load the node into a context.
  ctools_include('context');
  ctools_include('context-task-handler');
  $contexts = ctools_context_handler_get_task_contexts($task, '', array($node));

  $arg = array(isset($node->nid) ? $node->nid : $node->type);
  $output = ctools_context_handler_render($task, '', $contexts, $arg);
  if ($output === FALSE) {
    module_load_include('inc', 'node', 'node.pages');
    
    $context = reset($contexts);
    $output = ctools_pages_get_form($node->type.'_node_form', array($node), array('module' => 'node', 'file' => 'node.pages'));
  }

  return $output;
}

function ctools_pages_node_add_simple($type) {
  global $user;

  $types = node_type_get_types();

  // Initialize settings:
  $node = (object) array(
    'uid' => $user->uid,
    'name' => (isset($user->name) ? $user->name : ''),
    'type' => $type,
    'language' => LANGUAGE_NONE,
    'title' => null,
  );

  drupal_set_title(t('Create @name', array('@name' => $types[$type]->name)));
  return ctools_pages_node_edit_simple($node);
}

function ctools_pages_node_edit_simple_get_arguments($task, $subtask_id) {
  return array(
    array(
      'keyword' => 'node',
      'identifier' => t('Node being edited'),
      'id' => 1,
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
}

function ctools_pages_node_edit_simple_get_contexts($task, $subtask_id) {
  return ctools_context_get_placeholders_from_argument(ctools_pages_node_edit_simple_get_arguments($task, $subtask_id));
}

function ctools_pages_node_edit_simple_enable($cache, $status) {
  variable_set('ctools_pages_node_edit_simple_disabled', $status);
  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['page_manager_enabling_node_edit_simple'] = TRUE;
  }
}
