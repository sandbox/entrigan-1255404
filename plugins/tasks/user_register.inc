<?php

/*
 * @file
 * 
 * Plugin to allow page manager to take over the user register page.
 *
 */
function ctools_pages_user_register_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',

    'title' => t('User Register Page'),
    'admin title' => t('User Register Page'),
    'admin description' => t('When enabled, this overrides the default Drupal behavior for the user register page at <em>/user/register</em>.'),
    'admin path' => 'user/register',

    // Menu hooks so that we can alter the node/%node menu entry to point to us.
    'hook menu alter' => 'ctools_pages_user_register_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('ctools_pages_user_register_disabled', TRUE),
    'enable callback' => 'ctools_pages_user_register_enable',
  );
}

function ctools_pages_user_register_menu_alter(&$items, $task) {
  if (variable_get('ctools_pages_user_register_disabled', TRUE)) {
    return;
  }
  
  $callback = $items['user/register']['page callback'];
 
  // Override the menu item to point to our own function.
  if ($callback == 'drupal_get_form' || variable_get('page_manager_override_anyway', FALSE)) {
    $items['user/register']['page callback'] = 'ctools_pages_user_register';
    $items['user/register']['file path'] = $task['path'];
    $items['user/register']['file'] = $task['file'];
  }
  else {
    variable_set('ctools_pages_user_register_disabled', TRUE);
    if (!empty($GLOBALS['ctools_pages_enabling_user_register'])) {
      drupal_set_message(t('Page manager module is unable to enable the user register page because some other module already has overridden with %callback.', array('%callback' => $callback)), 'warning');
    }
    return;
  }

}

function ctools_pages_user_register() {
  // Load my task plugin
  $task = page_manager_get_task('user_register');

  ctools_include('context');
  ctools_include('context-task-handler');
  $output = ctools_context_handler_render($task, '', array(), array());
  if ($output !== FALSE) {
     return $output;
  }

  module_load_include('inc', 'user', 'user.pages');
  foreach (module_implements('page_manager_override') as $module) {
    $call = $module . '_page_manager_override';
    if (($rc = $call('user_register')) && function_exists($rc)) {
      $function = $rc;
      break;
    }
  }
  
  if(isset($function)){
    return $function;
  }
  // Otherwise, fall back.
  return drupal_get_form('user_register_form');
}

/**
 * Callback to enable/disable the page from the UI.
 */
function ctools_pages_user_register_enable($cache, $status) {
  variable_set('ctools_pages_user_register_disabled', $status);
  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['ctools_pages_enabling_user_register'] = TRUE;
  }
}
