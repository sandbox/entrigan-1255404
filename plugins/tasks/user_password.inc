<?php

/*
 * @file
 * 
 * Plugin to allow page manager to take over the user password page.
 *
 */
function ctools_pages_user_password_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',

    'title' => t('User Password Recovery Page'),
    'admin title' => t('User Password Recovery Page'),
    'admin description' => t('When enabled, this overrides the default Drupal behavior for the user password recovery page at <em>/user/pass</em>.'),
    'admin path' => 'user/pass',

    // Menu hooks so that we can alter the node/%node menu entry to point to us.
    'hook menu alter' => 'ctools_pages_user_password_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('ctools_pages_user_password_disabled', TRUE),
    'enable callback' => 'ctools_pages_user_password_enable',
  );
}

function ctools_pages_user_password_menu_alter(&$items, $task) {
  if (variable_get('ctools_pages_user_password_disabled', TRUE)) {
    return;
  }
  
  $callback = $items['user/password']['page callback'];

  // Override the menu item to point to our own function.
  if ($callback == 'drupal_get_form' || variable_get('page_manager_override_anyway', FALSE)) {
    $items['user/password']['page callback'] = 'ctools_pages_user_password';
    $items['user/password']['file path'] = $task['path'];
    $items['user/password']['file'] = $task['file'];
  }
  else {
    variable_set('ctools_pages_user_password_disabled', TRUE);
    if (!empty($GLOBALS['ctools_pages_enabling_user_password'])) {
      drupal_set_message(t('Page manager module is unable to enable poll because some other module already has overridden with %callback.', array('%callback' => $callback)), 'warning');
    }
    return;
  }

}

function ctools_pages_user_password() {
  // Load my task plugin
  $task = page_manager_get_task('user_password');

  ctools_include('context');
  ctools_include('context-task-handler');
  $output = ctools_context_handler_render($task, '', array(), array());
  if ($output !== FALSE) {
     return $output;
  }

  module_load_include('inc', 'user', 'user.pages');
  
  foreach (module_implements('page_manager_override') as $module) {
    $call = $module . '_page_manager_override';
    if (($rc = $call('user_password')) && function_exists($rc)) {
      $function = $rc;
      break;
    }
  }

  // Otherwise, fall back.
  return drupal_get_form('user_pass');
}

/**
 * Callback to enable/disable the page from the UI.
 */
function ctools_pages_user_password_enable($cache, $status) {
  variable_set('ctools_pages_user_password_disabled', $status);
  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['ctools_pages_enabling_user_password'] = TRUE;
  }
}
