<?php

/*
 * @file
 * 
 * Plugin to allow page manager to take over the user login page.
 *
 */
function ctools_pages_user_login_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',

    'title' => t('User Login Page'),
    'admin title' => t('User Login Page'),
    'admin description' => t('When enabled, this overrides the default Drupal behavior for the user login page at <em>/user/login</em>.'),
    'admin path' => 'user/login',

    // Menu hooks so that we can alter the node/%node menu entry to point to us.
    'hook menu alter' => 'ctools_pages_user_login_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('ctools_pages_user_login_disabled', TRUE),
    'enable callback' => 'ctools_pages_user_login_enable',
  );
}

function ctools_pages_user_login_menu_alter(&$items, $task) {
  if (variable_get('ctools_pages_user_login_disabled', TRUE)) {
    return;
  }
  
  //Since user/login is by default a local task, we need to do a little probing to find the callback.
  if($items['user/login']['type']==MENU_DEFAULT_LOCAL_TASK){
    $callback = $items['user']['page callback'];
  }
  else if(isset($items['user/login']['page callback'])){
    $callback = $items['user/login']['page callback'];
  }
  else{
    $callback = 'an unknown callback';
  }
  
  // Override the menu item to point to our own function.
  if ($callback == 'user_page' || variable_get('page_manager_override_anyway', FALSE)) {
    $items['user/login']['page callback'] = 'ctools_pages_user_login';
    $items['user/login']['file path'] = $task['path'];
    $items['user/login']['file'] = $task['file'];
  }
  else {
    variable_set('ctools_pages_user_login_disabled', TRUE);
    if (!empty($GLOBALS['ctools_pages_enabling_user_login'])) {
      drupal_set_message(t('Page manager module is unable to enable poll because some other module already has overridden with %callback.', array('%callback' => $callback)), 'warning');
    }
    return;
  }

}

function ctools_pages_user_login() {
  // Load my task plugin
  $task = page_manager_get_task('user_login');

  ctools_include('context');
  ctools_include('context-task-handler');
  $output = ctools_context_handler_render($task, '', array(), array());
  if ($output !== FALSE) {
     return $output;
  }

  module_load_include('inc', 'user', 'user.pages');
  $function = 'user_page';
  foreach (module_implements('page_manager_override') as $module) {
    $call = $module . '_page_manager_override';
    if (($rc = $call('user_login')) && function_exists($rc)) {
      $function = $rc;
      break;
    }
  }

  // Otherwise, fall back.
  return $function();
}

/**
 * Callback to enable/disable the page from the UI.
 */
function ctools_pages_user_login_enable($cache, $status) {
  variable_set('ctools_pages_user_login_disabled', $status);
  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['ctools_pages_enabling_user_login'] = TRUE;
  }
}
