<?php

/**
 * @file
 * Plugin to provide access control based upon role membership.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Node: Has ID"),
  'description' => t('Will be false if the node object has no id (e.g. node/add forms)'),
  'callback' => 'ctools_pages_node_has_id_ctools_access_check',
  'default' => array(false),
  'settings form' => 'ctools_pages_node_has_id_ctools_access_settings',
  'settings form submit' => 'ctools_pages_node_has_id_ctools_access_settings_submit',
  'summary' => 'ctools_pages_node_has_id_ctools_access_summary',
  'required context' => new ctools_context_required(t('Node'), 'node'),
);

function ctools_pages_node_has_id_ctools_access_settings($form, &$form_state, $conf) {
  return $form;
}

function ctools_pages_node_has_id_ctools_access_settings_submit($form, &$form_state) {
}

/**
 * Check for access.
 */
function ctools_pages_node_has_id_ctools_access_check($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }
  else{
    if(isset($context->data->nid) && $context->data->nid > 0){
      return TRUE;
    }
  }
}

/**
 * Provide a summary description based upon the checked roles.
 */
function ctools_pages_node_has_id_ctools_access_summary($conf, $context) {
  return 'Node has ID';
}
