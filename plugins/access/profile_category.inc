<?php

/**
 * @file
 * Plugin to provide access control based upon role membership.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Profile: category"),
  'description' => t('Control access by profile category.'),
  'callback' => 'ctools_pages_profile_category_ctools_access_check',
  'default' => array('rids' => array()),
  'settings form' => 'ctools_pages_profile_category_ctools_access_settings',
  'settings form submit' => 'ctools_pages_profile_category_ctools_access_settings_submit',
  'summary' => 'ctools_pages_profile_category_ctools_access_summary',
  'required context' => new ctools_context_required(t('Profile Category'), 'profile_category'),
);

/**
 * Settings form for the 'by role' access plugin
 */
function ctools_pages_profile_category_ctools_access_settings($form, &$form_state, $conf) {
  $form['settings']['categories'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Category'),
    '#default_value' => $conf['categories'],
    '#options' => ctools_pages_get_profile_categories(),
    '#description' => t('Only the checked categories will be granted access.'),
  );
  return $form;
}

/**
 * Compress the categories allowed to the minimum.
 */
function ctools_pages_profile_category_ctools_access_settings_submit($form, &$form_state) {
  $form_state['values']['settings']['categories'] = array_keys(array_filter($form_state['values']['settings']['categories']));
}

/**
 * Check for access.
 */
function ctools_pages_profile_category_ctools_access_check($conf, $context) {
  // As far as I know there should always be a context at this point, but this
  // is safe.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  $category = $context->data;
  return (bool) array_intersect($conf['categories'], array($category));
}

/**
 * Provide a summary description based upon the checked roles.
 */
function ctools_pages_profile_category_ctools_access_summary($conf, $context) {
  if (!isset($conf['categories'])) {
    $conf['categories'] = array();
  }
  $categories = ctools_pages_get_profile_categories();

  $names = array();
  foreach (array_filter($conf['categories']) as $cat) {
    $names[] = check_plain($categories[$cat]);
  }

  if (empty($names)) {
    return t('@identifier can have any category', array('@identifier' => $context->identifier));
  }

  return format_plural(count($names), '@identifier has category "@cats"', '@identifier has one of "@cats"', array('@cats' => implode(', ', $names), '@identifier' => $context->identifier));
}

function ctools_pages_get_profile_categories(){
  $categories = _user_categories();
  $names = array();
  foreach ($categories as $cat){
    $names[$cat['name']] = $cat['title'];
  }
  return $names;
}


