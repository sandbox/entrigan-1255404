<?php

/**
 * @file
 *
 * Plugin to provide a profile category form context
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Profile edit form"),
  'description' => t('A node edit form.'),
  'context' => 'ctools_pages_context_create_profile_category_form',
  'edit form' => 'ctools_pages_context_profile_category_form_settings_form',
  'defaults' => array('nid' => ''),
  'keyword' => 'profile_category',
  'context name' => 'profile_category_form',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the profile category for this argument:'),
  ),
);

/**
 * It's important to remember that $conf is optional here, because contexts
 * are not always created from the UI.
 */
function ctools_pages_context_create_profile_category_form($empty, $profile = NULL, $conf = FALSE) {

  static $created;
  
  //Setup argument parts
  $category = $profile['category'];
  $account = $profile['account'];
  
  $context = new ctools_context(array('form', 'profile_category', 'profile_category_form'));
  $context->plugin = 'profile_category_form';

  if ($empty || (isset($created) && $created)) {
    return $context;
  }
  $created = TRUE;

  if ($conf) {
    // In this case, $node is actually our $conf array.
    $category = is_string($category) ? $category : 0;

  }

  if (!empty($category)) {
    $form_id = 'user_profile_form';

    $form_state = array('want form' => TRUE, 'build_info' => array('args' => array($account, $category)));

    $file = drupal_get_path('module', 'user') . '/user.pages.inc';
    require_once DRUPAL_ROOT . '/' . $file;
    // This piece of information can let other modules know that more files
    // need to be included if this form is loaded from cache:
    $form_state['build_info']['files'] = array($file);

    $form = drupal_build_form($form_id, $form_state);

    // Fill in the 'node' portion of the context
    $context->data     = $category;
    $context->title    = $category;
    $context->argument = $category;

    $context->form       = $form;
    $context->form_state = &$form_state;
    $context->form_id    = $form_id;
    $context->form_title = $category;
    $context->restrictions['category'] = $category;
    $context->restrictions['form'] = array('form');
    return $context;
  }
}

function ctools_pages_context_profile_category_form_settings_form($form, &$form_state) {
  
  // @TODO this is completely untested.  Also should be converted to list or autocomplete field.
  $conf = &$form_state['conf'];

  $form['profile_category'] = array(
    '#title' => t('Enter the profile category'),
    '#type' => 'textfield',
    '#maxlength' => 512,
    '#weight' => -10,
  );

  return $form;
}

/**
 * Validate a node.
 */
function ctools_pages_context_profile_category_form_settings_form_validate($form, &$form_state) {
  
  // @TODO validate the value as a category type.
  
}

function ctools_pages_context_profile_category_form_settings_form_submit($form, &$form_state) {
  if ($form_state['values']['set_identifier']) {
    $form_state['values']['identifier'] = $form_state['values']['profile_category'];
  }

  // This will either be the value set previously or a value set by the
  // validator.
  $form_state['conf']['profile_category'] = $form_state['values']['profile_category'];
}
