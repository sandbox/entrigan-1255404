<?php

/**
 * @file
 * Plugin to handle the 'page' content type which allows the standard page
 * template variables to be embedded into a panel.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Node Edit Form'),
  'icon' => 'icon_node.png',
  'description' => t('The node edit form.'),
  'required context' => new ctools_context_required(t('Node'), 'node'),
  'category' => t('Forms'),
);

/**
 * Output function for the 'pane_tabs' content type.
 *
 * Outputs the tabs (local tasks) of the current page.
 */
function ctools_pages_node_edit_form_content_type_render($subtype, $conf, $panel_args, $context) {
  module_load_include('inc', 'node', 'node.pages');

  $node = isset($context->data) ? clone($context->data) : FALSE;
  $block = new stdClass();

  if ($node === FALSE) {
    return drupal_not_found();
  }
  
  $block->content = ctools_pages_get_form($node->type.'_node_form', array($node), array('module' => 'node', 'file' => 'node.pages'));
  return $block;
}

/**
 * Display the administrative title for a panel pane in the drag & drop UI
 */
function ctools_pages_node_edit_form_content_type_admin_title($subtype, $conf, $context) {
  return t('"@s" node edit form', array('@s' => $context->identifier));
}

function ctools_pages_node_edit_form_content_type_edit_form($form, &$form_state) {
  // provide a blank form so we have a place to have context setting.
  return $form;
}
