<?php

/**
 * @file
 *
 * Plugin to provide an argument handler for a Taxonomy term
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Profile Category"),
  // keyword to use for %substitution
  'keyword' => 'profile_category',
  'description' => t('Creates a profile context from a category machine name argument.'),
  'context' => 'ctools_pages_profile_category_context',
  'placeholder form' => array(
    '#type' => 'textfield',
    '#description' => t('Enter the profile category for this argument.'),
  ),
);

/**
 * Discover if this argument gives us the term we crave.
 */
function ctools_pages_profile_category_context($arg = NULL, $conf = NULL, $empty = FALSE) {
  // If unset it wants a generic, unfilled context.
  if ($empty) {
    return ctools_context_create_empty('profile_category_form');
  }
  if(is_array($arg) && is_string($arg['category'])){
    return ctools_context_create('profile_category_form', $arg);
  }
  
  return NULL;
}